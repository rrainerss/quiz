const questions = 
{
    1:
    {
        "question": "What is the square root of 9?",
        "a":4,
        "b":3,
        "c":17,
        "d":1,
        "correct-answer":"b",
    },
    2:
    {
        "question": "What is 3 + 3?",
        "a":5,
        "b":3,
        "c":9,
        "d":6,
        "correct-answer":"d",
    },
    3:
    {
        "question": "What is 3 to the power of 3?",
        "a":27,
        "b":20,
        "c":31,
        "d":7,
        "correct-answer":"a",
    },
};

// #region elements
const question = document.getElementById("question");
const answer_a = document.getElementById("answer_a");
const answer_b = document.getElementById("answer_b");
const answer_c = document.getElementById("answer_c");
const answer_d = document.getElementById("answer_d");
const next_button = document.getElementById("next_button");

function create_question()
{
    question.innerHTML = questions[question_counter]["question"];
    answer_a.innerHTML = questions[question_counter]["a"];
    answer_b.innerHTML = questions[question_counter]["b"];
    answer_c.innerHTML = questions[question_counter]["c"];
    answer_d.innerHTML = questions[question_counter]["d"];
}

// #endregion

let correct_answer_count = 0;
let question_counter = 0;

next_button.addEventListener("click", function()
{
    if(question_counter != 0)
    {
        var answer = document.querySelector('input[name="answer"]:checked').value;
        document.querySelector('input:checked').checked = false;
        check_answer(answer);
    }

    question_counter++;
    create_question();
});

function check_answer(answer)
{
    if(answer != null)
    {
        if(answer == questions[question_counter]["correct-answer"])
        {
            console.log('korrek');
            correct_answer_count++;
        }
        else
        {
            console.log('wong');
        }
        if(question_counter >= Object.keys(questions).length)
        {
            alert('correct Qs: ' + correct_answer_count);
        }
    }
}

/*
    if(question_counter > 0)
    {
        var answer = document.querySelector('input[name="answer"]:checked').value;
        console.log('ans' + answer);
        document.querySelector('input:checked').checked = false;

        if(answer == questions[question_counter]["correct-answer"])
        {
            if(question_counter < Object.keys(questions).length)
            {
                correct_answer_count++;
            }
            else
            {
                alert('Done! Correct: ' + correct_answer_count + 'questions');
            }
            
        }


    }*/


